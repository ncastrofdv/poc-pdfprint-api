import pdfrw

ANNOT_KEY = '/Annots'
ANNOT_FIELD_KEY = '/T'
ANNOT_VAL_KEY = '/V'
ANNOT_RECT_KEY = '/Rect'
SUBTYPE_KEY = '/Subtype'
WIDGET_SUBTYPE_KEY = '/Widget'

ticket = {"ticket": [
    [
        "MOVISTAR",
        "\n",
        "SERVICIO DE COBRANZA DE",
        "SALDO DE CELULAR",
        "\n",
        "Puesto:8861",
        "Empresa: 1488 MOVISTAR",
        "Nro.Op:130611492613932903",
        "Cod.Seg:A5FABE5523",
        "Forma de Pago Importe",
        "PESOS $50.00",
        "** TOTAL ** $50.00",
        "\n",
        "Numero de Celular: 1151658206",
        "\n",
        "Servicio cobrado por cuenta y orden",
        "de Movistar.",
        "Confirme su recarga llamando al *444.",
        "Para servicio al cliente marque *611.",
        "Conserve el presente como comprobante.",
        "Documento no valido como factura.",
        "Ud. Podra retirar su factura en",
        "cualquier oficina comercial de Movistar",
        "\n",
        "Emitido este comprobante no se",
        "aceptaran devoluciones ni reclamos",
        "00771151658206030001301306120170419115852",
        "\n",
        "ORIGINAL"
    ]
],
    "barra": "00771151658206030682000886120160801160218",
    "codResul": 0,
    "descResul": "OK",
}


def write_fillable_pdf():
    data_dict = {
        'company_name': 'Movistar',
        'ticket_body': [
            "MOVISTAR",
            "\n",
            "SERVICIO DE COBRANZA DE",
            "SALDO DE CELULAR",
            "\n",
            "Puesto:8861",
            "Empresa: 1488 MOVISTAR",
            "Nro.Op:130611492613932903",
            "Cod.Seg:A5FABE5523",
            "Forma de Pago Importe",
            "PESOS $50.00",
            "** TOTAL ** $50.00",
            "\n",
            "Numero de Celular: 1151658206",
            "\n",
            "Servicio cobrado por cuenta y orden",
            "de Movistar.",
            "Confirme su recarga llamando al *444.",
            "Para servicio al cliente marque *611.",
            "Conserve el presente como comprobante.",
            "Documento no valido como factura.",
            "Ud. Podra retirar su factura en",
            "cualquier oficina comercial de Movistar",
            "\n",
            "Emitido este comprobante no se",
            "aceptaran devoluciones ni reclamos",
            "00771151658206030001301306120170419115852",
            "\n",
            "ORIGINAL"
        ]
    }

    data_dict["ticket_body"] = " ".join(data_dict["ticket_body"])


    template_pdf = pdfrw.PdfReader("/app/api/prueba_spike_form.pdf")
    annotations = template_pdf.pages[0][ANNOT_KEY]
    for annotation in annotations:
        if annotation[SUBTYPE_KEY] == WIDGET_SUBTYPE_KEY:
            if annotation[ANNOT_FIELD_KEY]:
                key = annotation[ANNOT_FIELD_KEY][1:-1]
                if key in data_dict.keys():
                    annotation.update(
                        pdfrw.PdfDict(V='{}'.format(data_dict[key]))
                    )
    pdfrw.PdfWriter().write("/app/prueba_spike_filled.pdf", template_pdf)
