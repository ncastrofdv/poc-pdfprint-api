from flask import Flask, request, send_from_directory

from app.api.fill_pdf_service import write_fillable_pdf

app = Flask(__name__)


@app.route('/show/static-pdf/')
def show_static_pdf():
    write_fillable_pdf()
    return send_from_directory('/app/prueba_spike_filled.pdf', 'file.pdf')

