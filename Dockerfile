FROM python:3.6-alpine

RUN mkdir -p  app

COPY app /app
WORKDIR /app
ENV PYTHONPATH=/app

RUN pip install -r requirements.txt

ENV FLASK_DEBUG=1
ENV FLASK_RUN_PORT=7111
ENV FLASK_RUN_HOST=0.0.0.0
ENV FLASK_APP=api.app:app

CMD ["flask", "run"]